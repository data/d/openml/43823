# OpenML dataset: Heart-Disease-Prediction

https://www.openml.org/d/43823

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context: The leading cause of death in the developed world is heart disease. Therefore there needs to be work done to help prevent the risks of of having a heart attack or stroke.
Content: Use this dataset to predict which patients are most likely to suffer from a heart disease in the near future using the features given.
Acknowledgement: This data comes from the University of California Irvine's Machine Learning Repository at https://archive.ics.uci.edu/ml/datasets/Heart+Disease.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43823) of an [OpenML dataset](https://www.openml.org/d/43823). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43823/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43823/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43823/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

